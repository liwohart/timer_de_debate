package com.example.timerdedebate

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.google.android.material.floatingactionbutton.FloatingActionButton

class DebaterAdapter : Adapter<RecyclerView.ViewHolder>() {
    private val debaters: MutableList<Debater> = mutableListOf()

    fun addDebater(debaterName: String, debaterTimeBank: Minutes) {
        debaters.add(Debater(debaterName,debaterTimeBank))
        notifyItemInserted(itemCount - 1)
    }

    fun setTimeBank(debaterTimeBank: Minutes) {
        for (debater in debaters)
            debater.setTimeBank(debaterTimeBank,true)
        notifyItemRangeChanged(0,itemCount)
    }

    // override fun getItemId(position: Int): Long {
    //     // val currentDebater = debaters[position]
    //     return position.toLong()
    // }

    override fun getItemCount(): Int {
        return debaters.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val debaterView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.timer_card, parent,false)
        return DebaterViewHolder(debaterView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is DebaterViewHolder) {
            val currentDebater = debaters[position]

            currentDebater.setOnTick {
                currentDebater.setTimeBank(it)
                holder.debaterTimerView.text = currentDebater.formattedTime
                notifyDataSetChanged()
            }

            currentDebater.setOnFinish {
                currentDebater.clearTimeBank()
                holder.debaterTimerView.text = currentDebater.formattedTime
                notifyItemChanged(position)
            }

            holder.debaterButton.text = currentDebater.name.uppercase()
            holder.debaterButton.setOnClickListener {
                if (currentDebater.isSpeaking) currentDebater.stop()
                else currentDebater.start()
            }

            holder.debaterTimerView.text = currentDebater.formattedTime
            holder.debaterDeleteButton.setOnClickListener {
                currentDebater.stop()
                debaters.removeAt(position)
                // notifyDataSetChanged()
                notifyItemRemoved(position)
                notifyItemRangeChanged(position, itemCount)
            }
        }
    }

    class DebaterViewHolder(debaterView: View): RecyclerView.ViewHolder(debaterView){
        val debaterButton: Button = debaterView.findViewById(R.id.debater_timer_button)
        val debaterTimerView: TextView = debaterView.findViewById(R.id.debater_timer_view)
        val debaterDeleteButton: FloatingActionButton =
            debaterView.findViewById(R.id.debater_timer_delete_button)
    }
}