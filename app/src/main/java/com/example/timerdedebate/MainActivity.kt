package com.example.timerdedebate

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

typealias Minutes = Long
typealias Millis = Long

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpListeners()
        setUpRecyclerView()
    }

    private fun setUpListeners() {
        val addButton: FloatingActionButton = findViewById(R.id.add_button)
        val resetButton: FloatingActionButton = findViewById(R.id.reset_button)
        val timeBank: EditText = findViewById(R.id.time_bank)

        addButton.setOnClickListener {
            val debaterTimers: RecyclerView = findViewById(R.id.debater_timers)
            val inputDebater: EditText = findViewById(R.id.input_debater)
            val adapter = debaterTimers.adapter
            val debaterName: String = inputDebater.text.toString()
            val timeText: String = timeBank.text.toString()
            if (adapter is DebaterAdapter && debaterName != "" && timeText != "") {
                inputDebater.setText("")
                adapter.addDebater(debaterName,timeText.toLong())
                debaterTimers.scrollToPosition(adapter.itemCount - 1)
            }
        }

        resetButton.setOnClickListener {
            val debaterTimers: RecyclerView = findViewById(R.id.debater_timers)
            val adapter = debaterTimers.adapter
            val timeText: String = timeBank.text.toString()
            if (adapter is DebaterAdapter && timeText != "") {
                val debaterTime: Long = timeText.toLong()
                adapter.setTimeBank(debaterTime)
            }
        }
    }

    private fun setUpRecyclerView() {
        val debaterTimers: RecyclerView = findViewById(R.id.debater_timers)
        debaterTimers.layoutManager = LinearLayoutManager(this)
        debaterTimers.adapter = DebaterAdapter()
    }
}