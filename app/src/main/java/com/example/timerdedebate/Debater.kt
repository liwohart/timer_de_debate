package com.example.timerdedebate

import android.os.CountDownTimer

class Debater constructor(val name: String, timeInMinutes: Minutes) {
    private lateinit var timer: CountDownTimer
    private lateinit var _onTick: (millisLeft: Millis) -> Unit
    private lateinit var _onFinish: () -> Unit
    private var timeBank: Millis = timeInMinutes * 60000L
    private var speaking = false
    val isSpeaking get() = speaking
    val formattedTime get() =
        if (timeBank == 0L) "ESGOTADO"
        else "%02d:%02d".format( timeBank / 60000L, timeBank / 1000L % 60L)

    fun setTimeBank(newTimeBank: Long, fromMinutes: Boolean = false) {
        timeBank = if (fromMinutes) newTimeBank * 60000L else newTimeBank
    }

    fun clearTimeBank() = setTimeBank(0)

    fun setOnTick (callback : (millisLeft: Millis) -> Unit) { _onTick = callback }

    fun setOnFinish (callback : () -> Unit) { _onFinish = callback }

    fun start() = if (!speaking) {
        timer = object : CountDownTimer(timeBank, 1000L) {
            override fun onTick(p0: Millis) = _onTick(p0)
            override fun onFinish() = _onFinish()
        }
        timer.start()
        speaking = true
    } else Unit

    fun stop() = if (speaking) {
        timer.cancel()
        speaking = false
    } else Unit
}